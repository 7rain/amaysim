package promo;

public class PromoCode {
	
	private String code;
	private double discount;
	
	public PromoCode(String code, double discount) {
		this.setCode(code);
		this.setDiscount(discount);
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}
	
}
