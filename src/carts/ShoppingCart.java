package carts;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import items.Item;
import promo.PromoCode;
import rules.PricingRules;

public class ShoppingCart implements Cart {

	private ArrayList<Item> itemList;
	private ArrayList<Item> promoItems;
	private HashSet<PromoCode> promoCodeList;
	private PricingRules pricingRules;
	
	@SuppressWarnings("unused")
	private ShoppingCart() {
	}
	
	public ShoppingCart(PricingRules pricingRules) {
		// TODO Auto-generated constructor stub
		this.itemList = new ArrayList<Item>();
		this.promoItems = new ArrayList<Item>();
		this.promoCodeList = new HashSet<PromoCode>();
		this.pricingRules = pricingRules;
	}

	public void add(Item item) {
		// TODO Auto-generated method stub
		if (item != null) {
			itemList.add(new Item(item));
		}
	}

	public void add(Item item, PromoCode promoCode) {
		// TODO Auto-generated method stub
		if (item != null) {
			itemList.add(new Item(item));
		}
		if (promoCode != null) {
			promoCodeList.add(promoCode);
		}
	}
	
	public void applyRules() {
		if (this.getPricingRules() != null) {
			this.pricingRules.applyRules(this.itemList, this.promoItems);
		}
	}

	public void total() {
		// TODO Auto-generated method stub
		double total = 0;
		
		if (!this.itemList.isEmpty()) {
			Iterator<Item> itemIterator = this.itemList.iterator();
			while(itemIterator.hasNext()) {
				Item item = itemIterator.next();
				if (item != null) {
					total += item.getPrice();
				}
			}
		}
		
		if (!promoCodeList.isEmpty()) {
			double priceDiscount = 0;
			Iterator<PromoCode> promoIterator = promoCodeList.iterator(); 
			while (promoIterator.hasNext()) {
				PromoCode promo = promoIterator.next();
				priceDiscount += total * promo.getDiscount();
			}
			total -= priceDiscount;
		}
		
		System.out.println("===============");
		//DecimalFormat f = new DecimalFormat("##.00");
	    System.out.println("TOTAL: " + total);
	}

	public void items() {
		// TODO Auto-generated method stub
		
		System.out.println("cart items:");
		Iterator<Item> iterator = itemList.iterator();
		while(iterator.hasNext()) {
			Item item = iterator.next();
			if (item != null) {
				System.out.println(item.getName());
			}
		}
		
		System.out.println("promo items:");
		iterator = promoItems.iterator();
		while(iterator.hasNext()) {
			Item item = iterator.next();
			if (item != null) {
				System.out.println(item.getName());
			}
		}
		
	}

	public PricingRules getPricingRules() {
		return pricingRules;
	}

	public void setPricingRules(PricingRules pricingRules) {
		this.pricingRules = pricingRules;
	}

}
