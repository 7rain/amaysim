package carts;
import items.Item;
import promo.PromoCode;

public interface Cart {
	
	//public Cart create(PricingRules pricingRules);
	
	public void add(Item item);
	
	public void add(Item item, PromoCode promoCode);
	
	public void applyRules();
	
	public void total();
	
	public void items();
	
}
