import carts.Cart;
import carts.ShoppingCart;
import items.Item;
import promo.PromoCode;
import rules.BulkRule;
import rules.BundleRule;
import rules.DealRule;
import rules.PricingRules;

public class Main {

	public static void main(String[] args) {
		
		Item itemA = new Item("ult_small", "Unlimited 1GB", 24.90);
		Item itemB = new Item("ult_medium", "Unlimited 2GB", 29.90);
		Item itemC = new Item("ult_large", "Unlimited 5GB", 44.90);
		Item itemD = new Item("1gb", "1 GB Data-pack", 9.90);
		
		PricingRules pricingRules = new PricingRules();
		BundleRule bundleA = new BundleRule(itemB, itemD);
		DealRule dealA = new DealRule(itemA, 3);
		BulkRule bulkA = new BulkRule(itemC, 3, 39.90);
		
		// ADD RULES
		pricingRules.addRule(bundleA);
		pricingRules.addRule(dealA);
		pricingRules.addRule(bulkA);
		
		// SCENARIO 1
		Cart cart1 = new ShoppingCart(pricingRules);
		
		cart1.add(itemA);
		cart1.add(itemA);
		cart1.add(itemA);
		cart1.add(itemC);
		
		cart1.applyRules();
		cart1.items();
		cart1.total();
		
		// SCENARIO 2
		Cart cart2 = new ShoppingCart(pricingRules);
		
		cart2.add(itemA);
		cart2.add(itemA);
		cart2.add(itemC);
		cart2.add(itemC);
		cart2.add(itemC);
		cart2.add(itemC);
		
		cart2.applyRules();
		cart2.items();
		cart2.total();
		
		// SCENARIO 3
		Cart cart3 = new ShoppingCart(pricingRules);
		
		cart3.add(itemA);
		cart3.add(itemB);
		cart3.add(itemB);
		
		cart3.applyRules();
		cart3.items();
		cart3.total();
		
		// SCENARIO 4
		Cart cart4 = new ShoppingCart(pricingRules);
		
		PromoCode promoA = new PromoCode("I<3AMAYSIM", 0.10);
		
		cart4.add(itemA);
		cart4.add(itemD, promoA);
		
		cart4.applyRules();
		cart4.items();
		cart4.total();
		
	}
	
}
