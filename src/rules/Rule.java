package rules;

import java.util.ArrayList;

import items.Item;

public interface Rule {
	
	public boolean isMatch();
	
	public void apply(ArrayList<Item> itemList, ArrayList<Item> promoItems);

	public boolean getForFirstMonthOnly();
	
	public void setForFirstMonthOnly(boolean forFirstMonthOnly);
	
}
