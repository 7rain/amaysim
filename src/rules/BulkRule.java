package rules;

import java.util.ArrayList;
import java.util.Iterator;

import items.Item;

public class BulkRule implements Rule {

	private boolean forFirstMonthOnly;
	
	private Item mainItem;
	private int minimum;
	private double dropPrice;
	
	@SuppressWarnings("unused")
	private BulkRule() {
	}
	
	public BulkRule(Item mainItem, int minimum, double dropPrice) {
		this.mainItem = mainItem;
		this.minimum = minimum;
		this.dropPrice = dropPrice;
	}
	
	@Override
	public boolean isMatch() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean getForFirstMonthOnly() {
		// TODO Auto-generated method stub
		return this.forFirstMonthOnly;
	}

	@Override
	public void setForFirstMonthOnly(boolean forFirstMonthOnly) {
		// TODO Auto-generated method stub
		this.forFirstMonthOnly = forFirstMonthOnly;
	}


	@Override
	public void apply(ArrayList<Item> itemList, ArrayList<Item> promoItems) {
		// TODO Auto-generated method stub
		if (itemList != null) {
			int itemCounter = 0;
			boolean apply = false;
			Iterator<Item> iterator = itemList.iterator();
			while(iterator.hasNext()) {
				Item item = iterator.next();
				if (item.getCode().equals(this.mainItem.getCode())) {
					itemCounter++;
				}
				if (itemCounter > this.minimum) {
					apply = true;
					break;
				}
			}
			if (apply) {
				iterator = itemList.iterator();
				while(iterator.hasNext()) {
					Item item = iterator.next();
					if (item.getCode().equals(this.mainItem.getCode())) {
						item.setPrice(this.dropPrice);
					}
				}
			}
		}
	}

}
