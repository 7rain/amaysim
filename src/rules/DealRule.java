package rules;

import java.util.ArrayList;
import java.util.Iterator;

import items.Item;

public class DealRule implements Rule {

	private boolean forFirstMonthOnly;
	private Item mainItem;
	private int minimum;
	
	@SuppressWarnings("unused")
	private DealRule() {
		
	}
	
	public DealRule(Item mainItem, int minimum) {
		this.mainItem = mainItem;
		this.minimum = minimum;
	}
	
	@Override
	public boolean isMatch() {
		// TODO Auto-generated method stub
		return false;
	}
	

	@Override
	public boolean getForFirstMonthOnly() {
		// TODO Auto-generated method stub
		return this.forFirstMonthOnly;
	}

	@Override
	public void setForFirstMonthOnly(boolean forFirstMonthOnly) {
		// TODO Auto-generated method stub
		this.forFirstMonthOnly = forFirstMonthOnly;
	}


	@Override
	public void apply(ArrayList<Item> itemList, ArrayList<Item> promoItems) {
		// TODO Auto-generated method stub
		if (itemList != null) {
			int itemCounter = 0;
			Iterator<Item> iterator = itemList.iterator();
			while(iterator.hasNext()) {
				Item item = iterator.next();
				if (item.getCode().equals(this.mainItem.getCode())) {
					itemCounter++;
				}
				if (itemCounter == this.minimum) {
					System.out.println("set 0");
					item.setPrice(0);
					itemCounter = 0;
				}
			}
		}
	}

}
