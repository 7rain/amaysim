package rules;

import java.util.ArrayList;
import java.util.Iterator;

import items.Item;

public class BundleRule implements Rule {

	private boolean forFirstMonthOnly;
	private Item mainItem;
	private Item freeItem;
	
	public BundleRule(Item mainItem, Item freeItem) {
		this.setMainItem(mainItem);
		this.setFreeItem(freeItem);
	}
	
	@Override
	public boolean isMatch() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void apply(ArrayList<Item> itemList, ArrayList<Item> promoItems) {
		// TODO Auto-generated method stub
		if (itemList != null) {
			Iterator<Item> itemIterator = itemList.iterator();
			while (itemIterator.hasNext()) {
				Item item = itemIterator.next();
				if (item.getCode().equals(mainItem.getCode())) {
					promoItems.add(this.getFreeItem());
				}
			}
		}
	}	

	@Override
	public boolean getForFirstMonthOnly() {
		// TODO Auto-generated method stub
		return this.forFirstMonthOnly;
	}

	@Override
	public void setForFirstMonthOnly(boolean forFirstMonthOnly) {
		// TODO Auto-generated method stub
		this.forFirstMonthOnly = forFirstMonthOnly;
	}

	public Item getFreeItem() {
		return freeItem;
	}

	public void setFreeItem(Item freeItem) {
		this.freeItem = freeItem;
	}

	public Item getMainItem() {
		return mainItem;
	}

	public void setMainItem(Item mainItem) {
		this.mainItem = mainItem;
	}

}
