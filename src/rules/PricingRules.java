package rules;

import java.util.ArrayList;
import java.util.Iterator;
import items.Item;

public class PricingRules {

	 private ArrayList<Rule> ruleList;
	 
	 public PricingRules() {
		 this.ruleList =  new ArrayList<Rule>();
	 }
	 
	 public void addRule(Rule rule) {
		 this.ruleList.add(rule);
	 }
	 
	 public void applyRules(ArrayList<Item> itemList, ArrayList<Item> promoItems) {
		 if (!ruleList.isEmpty()) {
			 Iterator<Rule> iterator = ruleList.iterator();
			 while(iterator.hasNext()) {
				 Rule rule = iterator.next();
				 rule.apply(itemList, promoItems);
			 }
		 }
	 }
	 
}
